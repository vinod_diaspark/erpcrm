//
//  TabItem1View.swift
//  ERPCRM
//
//  Created by vinod on 5/27/20.
//  Copyright © 2020 Diaspark. All rights reserved.
//

import SwiftUI

struct TabItem1View: View {
    var body: some View {
        Text("Challenge")
    }
}

struct TabItem1View_Previews: PreviewProvider {
    static var previews: some View {
        TabItem1View()
    }
}
