//
//  LoginView.swift
//  ERPCRM
//
//  Created by vinod on 5/23/20.
//  Copyright © 2020 Diaspark. All rights reserved.
//

import SwiftUI

struct LoginView {
    
    @EnvironmentObject var user: User
    @State private var showHome = false
    @State private var showAlert = false

    
    func validUser()-> Bool {
        //user.userName = name
        //user.password = password
        
        if user.userName == "" {
            print("user is null")
        } else {
            print(user.userName)
        }
        
        return(UserModel.checkUser(user: user))  
          }
          
}

extension  LoginView: View {
    
    
    var body: some View {
        Group {
            if showHome {
                HomeView()
            } else {
                VStack{
                    // loginHeader()
                    HStack{
                        Text("Diaspark CRM ")
                        Text("")
                    }
                    VStack{
                        //TextField("Enter your name", text: $name)
                        //TextField("password", text: $password)
                        EnterNamePassword()
                        
                        
                    }
                    .padding()
                    
                    HStack{
                        Button(action: {
                            self.showHome = self.validUser()
                            self.showAlert = !self.showHome

                        }) {
                            Text("Ok")
                        }.alert(isPresented: $showAlert){
                            Alert(title: Text("Error"), message: Text("Wrong user code or password"))
                        }
                                
                                
                        .padding()
                    }
                    .padding()
                    
                    Text("Version 1.2")
                    
                }
            }
            // Text("Login screen")
        }
        
      
        
    }
    
    struct EnterNamePassword: View {
        @EnvironmentObject var user: User
    
        var body: some View {
            VStack {
                Text("Login")
                
                TextField("Enter your name", text: $user.userName)
                    .padding()
                    .cornerRadius(20.0)
                TextField("Password", text: $user.password)
                    .padding()
                    .cornerRadius(20.0)
                TextField("Company", text: $user.company)
                    .padding()
                    .cornerRadius(20.0)
                TextField("IP address", text: $user.ipAddress)
                    .padding()
                    .cornerRadius(20.0)
            }
            
        }
    }
}

struct Message: Identifiable {
    let id = UUID()
    let text: String
}

struct MasterView: View {
    @State private var message: Message? = nil

    var body: some View {
        VStack {
            Button("Show alert") {
                self.message = Message(text: "Hi!")
            }
        }.alert(item: $message) { message in
            Alert(
                title: Text(message.text),
                dismissButton: .cancel()
            )
        }
    }
}
    
    struct LoginView_Previews: PreviewProvider {
        static var previews: some View {
            LoginView()
                .environmentObject(User())
        }
}

