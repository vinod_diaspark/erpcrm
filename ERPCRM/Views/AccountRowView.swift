//
//  AccountRowView.swift
//  ERPCRM
//
//  Created by vinod on 6/12/20.
//  Copyright © 2020 Diaspark. All rights reserved.
//

import SwiftUI

import SwiftUI

struct AccountRowView: View {
   var accountMenu: MenuItem
  
  let checkmark = Image(systemName: "checkmark")
  
  var body: some View {
    self.buildView(accountMenu: accountMenu)
    //VStack{
    //    Image(accountMenu.mainImage)
    //    Text(accountMenu.name)
    //}
  }
    
    
    func buildView(accountMenu: MenuItem) -> AnyView {
        //return AnyView(TopCustomers())
        switch accountMenu.view {
           case  "TopCustomers" : return AnyView(TopCustomers())
           case "RecentCustomers" : return AnyView( RecentCustomers() )
           default: return AnyView(EmptyView())
        }
    }
}

struct AccountRowView_Previews: PreviewProvider {
  static var previews: some View {
    Text("asd")
    //AccountRowView(
    //  task: .constant( MenuItem(name: "To Do") )
   // )
  }
}


//struct ContentView : View {
//var myTypes: [Any] = [View1.self, View2.self]
//var body: some View {
//    List {
//        ForEach(0..<myTypes.count) { index in
//            self.buildView(types: self.myTypes, index: index)
//        }
//    }
//}
//
//func buildView(types: [Any], index: Int) -> AnyView {
//    switch types[index].self {
//       case is View1.Type: return AnyView( View1() )
//       case is View2.Type: return AnyView( View2() )
//       default: return AnyView(EmptyView())
//    }
//}

