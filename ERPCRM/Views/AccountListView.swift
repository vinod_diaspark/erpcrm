//
//  AccountListView.swift
//  ERPCRM
//
//  Created by vinod on 6/7/20.
//  Copyright © 2020 Diaspark. All rights reserved.
//

import SwiftUI

struct AccountListView: View{
   
  
    var choice: String
   
    @State var ShowwingDetail = false
    
    
    let menu = Bundle.main.decode([MenuSection].self, from: "customer.json")
    
    var body: some View{
       
        
        //NavigationView{
            
            List {
                ForEach (menu, id: \.id){section in
                    Section(header: Text(section.name)){
                        ForEach(section.items, id: \.id){item in
                            
                            NavigationLink(destination: AccountRowView(accountMenu: item)){
                             
                            Text(item.name)
                            }
                            // AccountRowView(accountMenu: item)
                            
                        }
                    }
                    
                }
            }.navigationBarTitle("customers")
            
                
            // }
        
        }
        }
        



struct AccountListView1: View {
    @State var ShowingDetail = false
    var choice: String
    var body: some View {
        //VStack{
          //  Text("Account List View: \(choice)")
        Button(action: {self.ShowingDetail.toggle()}){
            Text("Show detail")
        }.sheet(isPresented: $ShowingDetail){
            DetailView(detail: self.$ShowingDetail)
        }
    }
}

struct DetailView: View{
    @Binding  var detail: Bool
    
    var body: some View {
        VStack{
            Text("Detail")
            Button(action: {self.detail.toggle()}){
                Text("OK")
            }
        }
    }
}

//struct AccountListView_Previews: PreviewProvider {
//    static var previews: some View {
//        AccountListView(choice:"top", hideTabview:  false)
//    }
//}
