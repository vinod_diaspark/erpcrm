//
//  HellowWorld.swift
//  ERPCRM
//
//  Created by vinod on 6/11/20.
//  Copyright © 2020 Diaspark. All rights reserved.
//

import SwiftUI

struct HelloWorld: View {
     var choice: String
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
    }
}

struct HelloWorld_Previews: PreviewProvider {
    static var previews: some View {
        HelloWorld(choice:"hello, it's me")
    }
}
