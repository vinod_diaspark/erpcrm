//
//  AccountView.swift
//  ERPCRM
//
//  Created by vinod on 6/6/20.
//  Copyright © 2020 Diaspark. All rights reserved.
//

import SwiftUI

struct AccountView: View {
    @State var hideTabView = false
    var body: some View {
         NavigationView{
            VStack(spacing:30){
                Text("Accounts")
                NavigationLink(destination:AccountListView(choice:"top")){
                    Text("Top Customers")}
                NavigationLink(destination: AccountListView(choice:"recent")){
                    Text("Recent Customers")
                    
                }
            }  // end VStack
            .navigationBarTitle("Customers")
        }  // end NavigationView
    
        // Text("AccountView")
    }
}


struct AccountView_Previews: PreviewProvider {
    static var previews: some View {
        AccountView()
    }
}
