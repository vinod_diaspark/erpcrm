//
//  RecentCustomers.swift
//  ERPCRM
//
//  Created by vinod on 6/12/20.
//  Copyright © 2020 Diaspark. All rights reserved.
//

import SwiftUI

struct RecentCustomers: View {
    var body: some View {
        Text("Recent Customers")
    }
}

struct RecentCustomers_Previews: PreviewProvider {
    static var previews: some View {
        RecentCustomers()
    }
}
