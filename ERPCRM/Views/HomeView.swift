//
//  HomeView.swift
//  ERPCRM
//
//  Created by vinod on 5/27/20.
//  Copyright © 2020 Diaspark. All rights reserved.
//


import SwiftUI



struct HomeView {
    
    
    @State var selectedTab = 0
  
    
}

extension HomeView: View {
    
    var body: some View {
        NavigationView{
            
            Group {
                     if false {
                        Text("asdf")
                     } else {
  
            TabView {
                TabItem1View()
                    .tabItem({
                        VStack {
                            Image(systemName: "house")
                            Text("Home")
                        }
                    })
                    .tag(0)
           //         .navigationBarHidden(true)
                
                LoginView()
                    .tabItem({
                        VStack {
                            Image(systemName: "chart.bar")
                            Text("Task")
                        }
                    })
                    .tag(1)
             //       .navigationBarHidden(true)
                
                MusicView()
                    .tabItem({
                        VStack {
                            Image(systemName: "person")
                            Text("Music")
                        }
                    })
                    .tag(2)
               //     .navigationBarHidden(true)
                
                AccountListView(choice:"asdf")
                    .tabItem({
                        VStack {
                            Image(systemName: "person")
                            Text("Account")
                        }
                    })
                    .tag(3)
                 //   .navigationBarHidden(false)
                // .hidden()
                
            }
    // .hidden()
            .accentColor(.orange)
                        
        //.navigationBarTitle("nav crm")
       }
    }
        }
    }
}

#if DEBUG
struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
    }
}
#endif
