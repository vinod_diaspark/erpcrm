//
//  StarterView.swift
//  ERPCRM
//
//  Created by vinod on 5/23/20.
//  Copyright © 2020 Diaspark. All rights reserved.
//

import SwiftUI


struct StarterView {
  
   @EnvironmentObject var user: User
}

extension StarterView: View {
  
  var body: some View {
    Group {
      if self.user.isRegistered {
        LoginView()
            
      } else {
        LoginView()
            
      }
    }
  }
  
}

struct StarterView_Previews: PreviewProvider {
  static var previews: some View {
    StarterView()
      .environmentObject(User())
  }
}
