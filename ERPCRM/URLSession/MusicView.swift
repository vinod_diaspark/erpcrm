//
//  MusicView.swift
//  ERPCRM
//
//  Created by vinod on 6/21/20.
//  Copyright © 2020 Diaspark. All rights reserved.
//

import SwiftUI

struct MusicView: View {
  
  @State private var musicItems: [MusicItem] = []
  
  var body: some View {
    VStack {
      Button(action: fetchMusic) {
        Text("Fetch Music")
      }
      List(musicItems) { item in
        Text(item.trackName)
      }
      Spacer()
    }
  }
  
  func fetchMusic() {
    guard let url =  URL(string:"https://itunes.apple.com/search?media=music&entity=song&term=cohen") else {
      return
    }
    
//    URLSession Goes Here
    URLSession.shared.dataTask(with: url) { data, response, taskError in
      guard let httpResponse = response as? HTTPURLResponse,
        (200..<300).contains(httpResponse.statusCode),
        let data = data else {
          fatalError()
      }
      let decoder = JSONDecoder()
      guard let response = try? decoder.decode(MediaResponse.self, from: data) else {
        return
      }
      DispatchQueue.main.async {
        self.musicItems = response.results
      }
      
    }.resume()
    
//    Add JSON Decoding in URLSession closure
      
//    let decoder = JSONDecoder()
//    guard let response = try? decoder.decode(MediaResponse.self, from: data) else {
//      return
//    }
      
//    Update music items on main thread
//    DispatchQueue.main.async {
//      self.musicItems = response.results
//    }
    
  }
}


struct MusicView_Previews: PreviewProvider {
  static var previews: some View {
      MusicView()
  }
}




//import SwiftUI
//
//struct MusicView: View {
//    var body: some View {
//        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
//    }
//}
//
//struct MusicView_Previews: PreviewProvider {
//    static var previews: some View {
//        MusicView()
//    }
//}
