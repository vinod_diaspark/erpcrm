//
//  Profile.swift
//  ERPCRM
//
//  Created by vinod on 5/23/20.
//  Copyright © 2020 Diaspark. All rights reserved.
//
struct Profile {
  /// (Selected) name) of the learner.
  var name: String
  
  /// Initializes a new `Profile` with an empty `name`.
  init() {
    self.name = ""
  }
  
  /// Initializes a new `Profile` with a specified name.
  ///  - Parameters:
  ///     - name Name of the user profile.
  init(named name: String) {
    self.name = name
  }
}
