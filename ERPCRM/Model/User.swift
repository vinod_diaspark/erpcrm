//
//  User.swift
//  ERPCRM
//
//  Created by vinod on 5/23/20.
//  Copyright © 2020 Diaspark. All rights reserved.
//
import Combine

class User {
    
    @Published var isRegistered: Bool = false
    @Published var userName = ""
    @Published var password = ""
    @Published var company = ""
    @Published var ipAddress = ""
  
  var profile: Profile = Profile() {
    willSet {
      willChange.send(self)
    }
  }
  
  internal let willChange = PassthroughSubject<User, Never>()
  
  init() {
    self.profile.name = ""
  }
  
  init(name: String) {
    self.profile.name = name
  }
    
    
    
}
